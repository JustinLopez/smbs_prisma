import { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {
        if (req.method === 'GET') {
            const coaches = await prisma.coach.findMany({
                include: {
                    course: true,
                    CoachTopic: {
                        include: {
                            topic: {
                                include: {
                                    schedules: true
                                }
                            }
                        }
                    }
                }
            });
            res.status(200).json(coaches);
        } else {
            res.status(405).json({ error: 'Method Not Allowed' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error fetching coaches' });
    }
}
