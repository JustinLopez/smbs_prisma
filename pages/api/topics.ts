import { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {
        if (req.method === 'GET') {
            const topics = await prisma.topic.findMany({
                include: {
                    course: true,
                    schedules: true,
                    CoachTopic: {
                        include: {
                            coach: true
                        }
                    }
                }
            });
            res.status(200).json(topics);
        } else {
            res.status(405).json({ error: 'Method Not Allowed' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error fetching topics' });
    }
}
