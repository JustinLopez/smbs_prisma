import type { NextApiRequest, NextApiResponse } from 'next';
import { prisma } from '../../lib/prisma';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {
        if (req.method === 'GET') {
            const courses = await prisma.course.findMany({
                include: {
                    coaches: true,
                    topics: true
                }
            });
            res.status(200).json(courses);
        } else {
            res.status(405).json({ error: 'Method Not Allowed' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error fetching courses' });
    }
}